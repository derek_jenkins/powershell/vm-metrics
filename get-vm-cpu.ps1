$vms = Get-VM $args 
$a = Get-Stat -Stat 'cpu.demand.average','cpu.usagemhz.average' -Realtime -Entity $vms -IntervalMins 1 -Start ([datetime]::Now).AddMinutes(-1)
$vm = $a.Entity.Name | Sort-Object -Unique
$metrics = @()

Foreach ($v in $vm) {
    $stats = @()
    $time = ($a | ?{$_.Entity.Name -eq $v} | Sort-Object Timestamp | Sort-Object Timestamp -Unique | Select -Last 1).Timestamp
    $stats += $a | ?{$_.Entity.Name -eq $v -and $_.Timestamp -eq $time -and $_.MetricId -eq 'cpu.usagemhz.average'} | Sort-Object Value | Select -Last 1
    $stats += $a | ?{$_.Entity.Name -eq $v -and $_.Timestamp -eq $time -and $_.MetricId -ne 'cpu.usagemhz.average'} | Sort-Object Value | Select -Last 1
    $stats | %{$m = $_.MetricId; $_ | select @{N="entity"; E={$_.Entity.Name}}, @{N="$($m)"; E={$_.Value}}, @{N="unit"; E={$_.Unit}}, @{N="timestamp"; E={$_.Timestamp.DateTime}}} | %{$metrics += $_}
}

return $metrics | Select entity, 'cpu.usagemhz.average', 'cpu.demand.average', unit, timestamp | ft -AutoSize